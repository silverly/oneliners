Computers with most sessions
```
MATCH p=((S:Computer)-[r:HasSession]->(T:User))
WITH S.name as s,
COUNT(DISTINCT(T)) as t
RETURN {Name: s, Count: t} as MyResult
ORDER BY t DESC
LIMIT 10
```