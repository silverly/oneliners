When on an internal and you want to find all the DC in the environment (windows).

Windows box:
```
nslookup
set type=all
_ldap._tcp.dc._msdcs.Domain_Name
```

Linux Box:
```
nslookup
set type=srv
_ldap._tcp.dc._msdcs.Domain_Name
```