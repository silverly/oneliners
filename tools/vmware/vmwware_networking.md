When you want to turn Win10 into a router:
E.g. Kali ---> Win10 VM ---> VPN to hacked network
You need to of course setup routing in Kali

```
New-NetNat -Name NatNetwork -InternalIPInterfaceAddressPrefix 192.168.1.0/24
```

For Port Forwarding back
```
Add-NetNatStaticMapping -NatName NatNetwork -Protocol TCP -ExternalIPAddress 0.0.0.0 -InternalIPAddress 192.168.1.123 -InternalPort 80 -ExternalPort 80
```

Keep in mind, they are persistent settings.
```
Remove-NetNat *; Remove-NetNatStaticMapping *
```