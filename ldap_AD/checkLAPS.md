The best way to check for laps in the environment (if windows), is to check the domain controller, with a user account and via ldap search use

```
ldapsearch -b $THEDOMAINBINDNAME_eg"dn=silverly,dn=com,dn=au" -D $the_domain_user_eg"silverly@silverly.com.au" -W -x -h $dcserver -s sub "cn=ms-msc-admpwd,cn=Schema,cn=Configuration"
```

If you have domain account / domain joined computer

```
get-adobject "cn=ms-msc-admpwd,cn=Schema,cn=Configuration,dn=silverly,dn=com,dn=au"
```

Extracting laps from a workstation given you have enough access
```
ldapsearch -x -h <DC IP> -D "userwithacl/DA" -w <password> -b "dc=ldapbase,dc=local" "(ms-MCS-AdmPwd=*)" ms-MCS-AdmPwd
```